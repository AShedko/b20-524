#include <stdio.h>  // import print ...
#include "stdlib.h" // import ...
#include "stdbool.h"

int a1(int arg) { return arg + 1; }

int a2(int arg) { return arg + 2; }

int a3(int arg) { return arg + 3; }

int a4(int arg) { return arg + 4; }

int a5(int arg) { return arg + 5; }

int (*funs[5])(int) = {
        a1,
        a2,
        a3,
        a4,
        a5,
};

void *get_fun(int f_id) {
    return funs[f_id];
}

struct fun {
    void *(*call)(struct fun *, void *);
};

typedef struct fun fun;

#define CALL(f, x) f->call(f,x)

struct comp {
    void *(*call)(struct comp *, void *);
    fun *f;
    fun *g;
};

typedef struct comp comp;

void *compose_call(comp *this, void *x) {
    void* r1 = CALL(this->f, x);
    void* r2 = CALL(this->g, r1);
    free(r1); // free temporary
    return r2;
}

fun *compose(fun *f, fun *g) {
    comp *result = malloc(sizeof(comp));
    result->call = &compose_call;
    result->f = f;
    result->g = g;
    return (fun *) result;
}

void *where(bool f(void *), void *ar, int size) {
    float *ret = malloc(size * 4); // replace with construct

    int j = 0;

    for (int i = 0; i < size; ++i) {
        if (f((void *)
                      (char *) ar + i * sizeof(float)
        )) {
            ret[j++] = ((float *) (ar))[i]; // replace with append
        }
    }
    return ret; // return data + length
}

bool ge5(void *arg) {
    return (*(float *) arg) >= 5;
}

void* five_adder(fun* ff, void* f) {
    double* d = malloc(sizeof(double));
    *d = *(double*) f + 5.0;
    return d;
}

void* five_divider(fun* ff, void* f) {
    double* d = malloc(sizeof(double));
    *d = *(double*) f / 5.0;
    return d;
}

int main(int argc, char **argv) {
    printf("%d\n", ((int (*)(int f)) get_fun(2))(2));

    void *ar = malloc(5 * sizeof(float));
    ((float *) ar)[0] = 54.0f;
    ((float *) ar)[1] = 54.0f;
    ((float *) ar)[2] = 52.0f;
    ((float *) ar)[3] = .1f;
    ((float *) ar)[4] = .3f;

    void *ret = where(ge5, ar, 5);
    printf("%f\n", ((float *) ret)[2]);

    fun* adder = malloc(sizeof(fun));
    adder->call = five_adder;
    fun* divider = malloc(sizeof(fun));
    divider->call = five_divider;
    fun* comp = compose(adder, divider);
    double arg = 5.0;
    printf("%f\n", *(double*)CALL(comp, &arg)); /* prints "2" */
    free(adder);
    free(divider);
    free(comp);
    return 0;
}
