//
// Created by ashedko on 02.03.2021.
//

#ifndef B524_GENERIC_VECTOR_H
#define B524_GENERIC_VECTOR_H

#include "stdint.h"

struct EL // Group element
{
    void *(*sum)(void *, void *); // Addition
    void *(*inv)(void *);         // inverse
    void *(*zero)(); // Neutral element
    void *(*one)();  // Base
    uint8_t size;
};

typedef struct EL EL;

void *fsum(void *a, void *b);

void *isum(void *a, void *b);

void *finv(void *a);

void *iinv(void *a);

static float FLOAT_ZERO = 0.0f;
static float FLOAT_ONE = 1.0f;

static float INT_ZERO = 0.0f;
static float INT_ONE = 1.0f;

void *fzero();

void *izero();

void *fone();

void *ione();

static EL FLT_Type = {fsum, finv, fzero, fone, sizeof(float)};
static EL INT_Type = {isum, iinv, izero, ione, sizeof(int)};

struct generic_vector {
    int size;
    int capacity;
    char *data;
    EL *type;
};
typedef struct generic_vector generic_vector;

void *ith(generic_vector V, int i);

void *reduce(generic_vector V);

#endif //B524_GENERIC_VECTOR_H
