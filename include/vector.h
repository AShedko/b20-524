#ifndef B524_VECTOR_H
#define B524_VECTOR_H

struct Vector3
{
    float x;
    float y;
    float z;
};
typedef struct Vector3 Vector3;

struct Vector3 sum3(struct Vector3 v1, struct Vector3 v2);

struct VectorN
{
    int dimension;
    float *coordinates;
};
typedef struct VectorN VectorN;

struct VectorN* sumN(struct VectorN *v1, struct VectorN *v2);

#endif //B524_VECTOR_H
