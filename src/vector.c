#include "vector.h"

#include <stdlib.h>

struct Vector3 sum3(struct Vector3 v1, struct Vector3 v2) {
    struct Vector3 v3;
    v3.x = v1.x + v2.x;
    v3.y = v1.y + v2.y;
    v3.z = v1.z + v2.z;
    return v3;
}

struct VectorN *sumN(struct VectorN *v1, struct VectorN *v2) {
    if (v1->dimension != v2->dimension) // = (*v1).dimension
        return NULL ;// throw an exception
    struct VectorN *v3 = malloc(sizeof(struct VectorN)); // sizeof(struct VectorN) = 8 (!)
    v3->dimension = v1->dimension;
    v3->coordinates = (float *) malloc(sizeof(float) * v3->dimension);
    for (int i = 0; i < v3->dimension; ++i)
        *(v3->coordinates + i) = *(v1->coordinates + i) + *(v2->coordinates + i);// v2->coordinates[i]
    return v3;
}

float f() {
    float v1[2] = {1, 2};
    float v2[2] = {-1, -2};
    struct VectorN v11 = {2, v1};
    struct VectorN v22 = {2, v2};
    sumN(&v11, &v22);
    struct VectorN vector;

    *(vector.coordinates + 3) = 123.;
    float f = *(vector.coordinates + 3);
    return f;
}