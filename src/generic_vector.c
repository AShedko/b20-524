//
// Created by ashedko on 02.03.2021.
//

#include "generic_vector.h"
#include "assert.h"
#include "stdlib.h"

void *ith(generic_vector V, int i){
    assert(i >= 0 && i < V.size);
    return V.data + V.type->size * i;
}
//void *reduce(generic_vector V, *void*(void*,void*) fun, void* zero);
void *reduce(generic_vector V) {
    void *zero = V.type->zero();
    void *acc = zero;
    for (int i = 0; i < V.size; ++i) {
        acc = V.type->sum(acc, ith(V,i)); // How to fix this leak?
    }
    return acc;
}

void *fsum(void *a, void *b) {
    float *res = (float *) malloc(sizeof(float));
    res[0] = *(float *) a + *(float *) b;
    return res;
};

void *isum(void *a, void *b) {
    int *res = (int *) malloc(sizeof(int));
    res[0] = *(int *) a + *(int *) b;
    return res;
};

void *finv(void *a) {
    float *res = (float *) malloc(sizeof(float));
    res[0] = -*(float *) a;
    return res;
};

void *iinv(void *a) {
    int *res = (int *) malloc(sizeof(int));
    res[0] = -*(int *) a;
    return res;
};


void *fzero() { return &FLOAT_ZERO; };
void *izero() { return &INT_ZERO; };

void *fone() { return &FLOAT_ONE; };
void *ione() { return &INT_ONE; };