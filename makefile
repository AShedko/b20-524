BIN_DIR=bin
OBJ_DIR=obj
I_DIR="include"
S_DIR=src
CC = gcc
CFLAGS = -I $(I_DIR)

# todo add other seminars ELF = Executable and Linkable Format
results = $(BIN_DIR)/seminar_01.elf $(BIN_DIR)/seminar_02.elf $(BIN_DIR)/seminar_03.elf $(BIN_DIR)/seminar_04.elf

.PHONY : all
all: $(results)

$(BIN_DIR)/seminar_01.elf: seminar_01.c | $(BIN_DIR)  # $@ -- target name
	$(CC) $(CFLAGS) seminar_01.c -o $@ # compile with default compiler and default flags

$(BIN_DIR)/seminar_02.elf: seminar_02.c $(OBJ_DIR)/vector.o | $(BIN_DIR) $(OBJ_DIR)
	$(CC) $(CFLAGS) seminar_02.c $(OBJ_DIR)/vector.o -I . -o $@

$(BIN_DIR)/seminar_03.elf: seminar_03.c $(OBJ_DIR)/generic_vector.o | $(BIN_DIR) $(OBJ_DIR)
	$(CC) $(CFLAGS) seminar_03.c $(OBJ_DIR)/generic_vector.o -o $@

$(BIN_DIR)/seminar_04.elf: seminar_04.c | $(BIN_DIR)
	$(CC) $(CFLAGS) seminar_04.c -o $@

# $< -- first item in deps list
$(OBJ_DIR)/%.o: src/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) -c  $< -o $@

$(BIN_DIR):
	mkdir -p $@

$(OBJ_DIR):
	mkdir -p $@

.PHONY : clean
clean :
	rm -rf $(BIN_DIR)
	rm -rf $(OBJ_DIR)
