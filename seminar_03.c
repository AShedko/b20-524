#include <stdio.h>  // import print ...
#include <stdlib.h> // import ...
#include "include/generic_vector.h"

#define inc(a) ((a) + 1)

void *mul_inv(void *a) {
    float *res = (float *) malloc(sizeof(float));
    *res = 1.0f / *(float *) a;
    return res;
};

void *mul(void *a, void *b) {
    float *res = (float *) malloc(sizeof(float));
    *res = *(float *) a * *(float *) b;
    return res;
};
static float FLT_BASE = 2.0f;

void *mul_base() { return &FLT_BASE; }


int main(int argc, char **argv) {
    float *p = calloc(sizeof(float), 2);
    p[0] = 2.0f;
    p[1] = 4.0f;
    EL MUL_FLT_Type = {.sum=mul, .inv=mul_inv, .zero = fone, .one= mul_base, sizeof(float)};

    generic_vector V = {.size=2, .capacity=2, .data=(char *) (p), &FLT_Type};

    printf("Sum is %f\n", *(float *) reduce(V));
    return 0;
}
