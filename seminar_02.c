#include <stdio.h>  // import print ...
#include <stdlib.h> // import ...
#include "include/vector.h"

long int fib(int n) {
    if (n <= 1) { return 1; }
    return fib(n - 1) + fib(n - 2);
}

int main(int argc, char **argv) {

// Formatting convention:
//    struct -> CamelCase
//    function -> snake_case
//    variable -> snake_case
//    const -> SCREAMING_SNAKE
//    MACRO -> SCREAMING_SNAKE

    puts("Fibonacci numbers:");
    for (int i = 0; i < 3; i++) {
        printf("fib(%d) = %ld\n", i+1, fib(i));
    }

    Vector3 v1 = {1.0f, -1.0f, 222.5f};
    Vector3 vv = {.x=12.f, .y=432,.z=23};
    Vector3 v2 = {-1.5f, 1.0f, -222.5f};

    printf("X of v1 + v2 is %f\n", sum3(v1, v2).x);
    return 0;
}
