#include <stdio.h>  // import print ...
#include "stdlib.h" // import ...

struct A {
    int a;
    int *b;

    int (*sum)(int, int);
};

//plain old data = POD
typedef struct A A;
struct B {
    A a;
    char h;
};

/*
class A:
    def __init__(self, a:Int, b: Int, f):
        self.a = a
        self.b = b
        self.sum = f
 */

int reduce(int(*sum)(int, int), int *list, size_t size, int zero) { // def reduce(sum, list, zero)
    int res = zero; // res =zero
    for (int i = 0; i < size; i++) // for (i in range(list.size)):
    {
        res = sum(res, list[i]);
    }
    return res;
}

int sum(int, int);    // declaration
int sum(int a, int b) // definition
{
    return a + b;
}

/*
def sum(a,b):
    return a + b
*/

int sub(int, int); // declaration
int sub(int a, int b)// definition
{
    return a - b;
}

int main(int argc, char **argv) {
    int c = 7;
    A a = {.b=&c, .a=2, .sum=sub}; // does not work in c++, yet

    int ar[4] = {1, 2, 4, 8};
    int ar2[4] = {[3]=8}; // does not work in c++, yet

    printf("reduce(sub, ar, 4, 10) gives: %i\n", reduce(sub, ar, 4, 10));
    puts("which is 10 - 1 - 2 - 4 - 8 ");
    return 0;
}

/*
def main(argv):
    a = A(2,4,sum)
    a.sum(a.a,a.b)
    print(a)
    return 0

if __name__ == 'main':
    main(sys.argv)
 */

